### GTK

Easy to integrate Python logging to GLib logging to get unified look:

```py
def _log(level: GLib.LogLevelFlags, message: str):
    variant_message = GLib.Variant('s', message)

    variant_dict = GLib.Variant('a{sv}', {
        'MESSAGE': variant_message,
    })
    GLib.log_variant(SHORT_NAME, level, variant_dict)


# Logbook custom handler to redirect message to GLib log
class GLibLogHandler(Handler, StringFormatterHandlerMixin):
    def emit(self, record):
        message = self.format(record)
        level = LOGBOOK_LEVEL_TO_GLIB[record.level]
        _log(level, message)
```
