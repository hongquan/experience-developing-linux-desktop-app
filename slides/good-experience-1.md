## Good experience

- Python binding is pretty mature.

- All libraries which power desired features have Python binding: Zbar, Gstreamer, NetworkManager.

- Documentation is a bit out-dated, but I get the support from community on undocumented features.
