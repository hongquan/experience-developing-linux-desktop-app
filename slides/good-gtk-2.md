### GTK

- GTK and underlying libraries are designed to facilitate adding language bindings: GObject Introspection.
- Many bindings in other languages with high level of completion: 8 official languges and many unofficial.
- Even an unofficial, young binding like Rust has many applications in production.
