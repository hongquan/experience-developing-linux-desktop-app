### GTK

- To compare, Qt has only one official language binding: Python
- Founded a framework and inspired many libraries to follow:

  + GStreamer
  + BlueZ
  + NetworkManager

- Auto-generated Python API documentation: https://lazka.github.io/pgi-docs/ (298 libraries!)
