### ZBar

- https://github.com/mchehab/zbar

- A big surprise, because a complete library exists before application.

- ZBar tries to be full-featured: providing GTK widget, Qt widget. But they require X11.

- I only use the core part of ZBar: decoding QR code from image buffer.
