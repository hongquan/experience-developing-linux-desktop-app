
## Who am I

- Free and Open-Source Software enthusiast.
- Software developer in web backend (Python), embedded devices (Arduino), wifi router, desktop.
- [Google Summer of Code](https://developers.google.com/open-source/gsoc/) mentor.
- Co-founder & CTO of [AgriConnect](http://agriconnect.vn) 🇻🇳.
