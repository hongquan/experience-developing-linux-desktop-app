## What is it?

What the app is for:

- I want to scan QR code with my laptop.

- Can scan the code with my webcam, or scan from an image file.

- Run natively in Wayland desktop (no XWayland involved).
