### GStreamer

- Can write down pipeline to test with CLI:

  ```sh
  gst-launch-1.0 v4l2src name=webcam_source ! tee name=t ! queue !
  glsinkbin sink="gtkglsink name=sink" name=sink_bin
  t. ! queue leaky=2 max-size-buffers=2 ! videoconvert ! video/x-raw,format=GRAY8 !
  appsink name=app_sink max_buffers=2 drop=1
  ```

- Implement in code: Just parse the above command

  ```py
  command = (f'v4l2src name={self.GST_SOURCE_NAME} ! tee name=t ! queue ! '
             f'glsinkbin sink="gtkglsink name={self.SINK_NAME}" name=sink_bin '
             't. ! queue leaky=2 max-size-buffers=2 ! videoconvert ! '
             'video/x-raw,format=GRAY8 ! '
             f'appsink name={self.APPSINK_NAME} max_buffers=2 drop=1')
  pipeline = Gst.parse_launch(command)
  ```
