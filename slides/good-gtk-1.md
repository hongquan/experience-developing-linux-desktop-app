### GTK

- GTK is not a single body GUI library. It is organized as many layers, each can be accessed from Python.

<p class='stretch'><img src='img/GTK_software_architecture.svg'></p>
