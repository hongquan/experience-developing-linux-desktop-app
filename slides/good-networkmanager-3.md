### NetworkManager


```py
from gi.repository import GLib, NM


def add_wifi_connection(info: WifiInfoMessage, callback: Optional[Callable], btn: Any, nm_client: Optional[NM.Client]):
    conn = NM.RemoteConnection()
    base = NM.SettingConnection.new()
    connection_name = f'{info.ssid} ({BRAND_NAME})'
    base.set_property(NM.SETTING_CONNECTION_ID, connection_name)
    conn.add_setting(base)
    ssid = GLib.Bytes.new(info.ssid.encode())
    wireless = NM.SettingWireless.new()
    wireless.set_property(NM.SETTING_WIRELESS_SSID, ssid)
    wireless.set_property(NM.SETTING_WIRELESS_HIDDEN, info.hidden)
    secure = NM.SettingWirelessSecurity.new()
    try:
        key_mn = NMWifiKeyMn[info.auth_type.name] if info.auth_type else None
    except KeyError:
        pass
    if key_mn:
        secure.set_property(NM.SETTING_WIRELESS_SECURITY_KEY_MGMT, key_mn)
    if info.password:
        if key_mn == NMWifiKeyMn.WPA:
            secure.set_property(NM.SETTING_WIRELESS_SECURITY_PSK, info.password)
        elif key_mn == NMWifiKeyMn.WEP:
            secure.set_property(NM.SETTING_WIRELESS_SECURITY_WEP_KEY0, info.password)
    conn.add_setting(wireless)
    conn.add_setting(secure)
    nm_client.add_connection_async(conn, True, None, callback, btn)
```
