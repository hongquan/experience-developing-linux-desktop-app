## Bad experience

- Sometimes documentation is out-dated, or lacking.
- GIO has bug with some rare HTTP URLs, leading me fallback to Python [`requests`](https://pypi.org/project/requests/).

- Diverse in distribution way:

  + Traditional: package manager (APT, dnf, pacman)
  + Container: Flatpak, Snap
