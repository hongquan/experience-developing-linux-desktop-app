### GIO

- Introduction: https://developer.gnome.org/gio/stable/ch01.html

- Unified API for on-disk file and remote file (HTTP, SFTP)

```py [3,8]
def process_passed_image_file(self, chosen_file: Gio.File):
    self.raw_result_buffer.set_text('')
    stream: Gio.FileInputStream = chosen_file.read(None)
    w, h = self.get_preview_size()
    scaled_pix = GdkPixbuf.Pixbuf.new_from_stream_at_scale(stream, w, h, True, None)
    self.insert_image_to_placeholder(scaled_pix)
    stream.seek(0, GLib.SeekType.SET)
    full_buf, etag_out = chosen_file.load_bytes()  # type: GLib.Bytes, Optional[str]
    self.process_passed_rgb_image(full_buf.get_data())
```
