### GStreamer

- Excellent multimedia framework

- Let imagine and implement flow of data quickly!

<p class='stretch'><img class='bg-white' src='img/gstreamer-diagram.svg'></p>

- Can expand to network video stream (RTSP, MPEG-DASH).
