## What is it?

Last year, I developed a QR scanner app for Linux, CoBang (https://github.com/hongquan/CoBang), as a hobby project.

From that I learn how rich, good-designed of libraries that Linux ecosystem offers.
