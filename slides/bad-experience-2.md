## Bad experience

- Flatpak favors different code layout:

  + Version string is defined in *meson.build* file.
  + Entry point script must be modified at build: No way to run the app in development
  + The build process is run inside some container: hard to debug.
  + Different layout for translation (_*.po_) files.

- Failed to release new version to FlatHub, stuck at building translation files to Flatpak.
