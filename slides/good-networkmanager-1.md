### NetworkManager

- If the QR code contain Wifi detail, we want to let connect to the network.
- We don't need to modify system settings ourselves, which has risk of causing conflict.
- Just communicate with NetworkManager, via D-Bus, a Linux IPC mechanism.
- We don't even need to talk D-Bus. There is GObject library to communicate with NetworkManager.
