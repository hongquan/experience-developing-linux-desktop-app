## What is it?

- If the QR code represents wifi detail, you can save and connect the network quickly.

  ![QR-Wifi](img/QR-Wifi.png)
