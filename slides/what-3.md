## What is it?

- If the QR code represents an URL, you can open browser quickly.

  ![QR-URL](img/QR-URL.png)
